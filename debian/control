Source: gprbuild
Section: devel
Priority: optional
Maintainer: Nicolas Boulenguez <nicolas@debian.org>
Uploaders: Ludovic Brenta <lbrenta@debian.org>
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Arch:
 dh-ada-library (>= 9.1),
 dh-sequence-ada-library,
 gnat,
 gnat-14,
 libxmlada-dom-dev,
 libxmlada-input-dev,
 libxmlada-sax-dev,
 libxmlada-schema-dev,
# libxmlada_unicode is required indirectly
Build-Depends-Indep:
 dh-sequence-sphinxdoc,
# Sphinx >= 1.6 uses the latexmk driver:
 latexmk,
 python3-sphinx,
# makeinfo:
 texinfo,
# documentation depends on tgtermes.sty:
 tex-gyre,
# ptmr8t.tfm:
 texlive-fonts-recommended,
# fncychap.sty:
 texlive-latex-extra,
Standards-Version: 4.7.2
Rules-Requires-Root: no
Homepage: https://github.com/AdaCore/gprbuild
Vcs-Browser: https://salsa.debian.org/debian/gprbuild
Vcs-Git: https://salsa.debian.org/debian/gprbuild.git

Package: libgnatprj12
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: support for programs processing GNAT projects (runtime)
 GNAT projects are human-readable text files used to drive tools
 building or inspecting lots of source files in several programming
 languages, like those provided by the gprbuild package.
 .
 This package contains the runtime shared library.

Package: libgnatprj-dev
Breaks: libgnatprj7-dev, libgnatprj8-dev, libgnatprj9-dev, libgnatprj10-dev
Replaces: libgnatprj7-dev, libgnatprj8-dev, libgnatprj9-dev, libgnatprj10-dev
Provides: ${ada:Provides}
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Suggests: gprbuild-doc
Description: support for programs processing GNAT projects (development)
 GNAT projects are human-readable text files used to drive tools
 building or inspecting lots of source files in several programming
 languages, like those provided by the gprbuild package.
 .
 This package contains the development tools.

Package: gprbuild
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
# 23~20220701-1 sets the now required Bindfile_Option_Substitution attribute
 gprconfig-kb (>= 23~20220701-1),
Suggests: gnat, g++, gcc, gfortran, gprbuild-doc
Description: multi-language extensible build tool
 A set of tools for processing GNAT project files:
 gprconfig detects available compilers,
 gprbuild runs them;
 gprslave helps distributing the build work across the network;
 gprinstall copies the objects to their final destination;
 gprclean removes them.
 The default configuration supports Ada, Assembler, C, C++, Fortran,
 and can be extended to support user source processing tools.

Package: gprbuild-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${sphinxdoc:Depends},
Built-Using: ${sphinxdoc:Built-Using}
Suggests: gprbuild
# Examples were in the gprbuild package when there was no -doc package.
Replaces: gprbuild (<< 2021.0.0.0778b109-1)
Breaks: gprbuild (<< 2021.0.0.0778b109-1)
Description: multi-language extensible build tool (documentation)
 A set of tools for processing GNAT project files:
 gprconfig detects available compilers,
 gprbuild runs them;
 gprslave helps distributing the build work across the network;
 gprinstall copies the objects to their final destination;
 gprclean removes them.
 The default configuration supports Ada, Assembler, C, C++, Fortran,
 and can be extended to support user source processing tools.
 .
 This package contains the documentation.
